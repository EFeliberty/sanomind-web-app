// Global Variables - TESTING
var categoryArray = new Array(' ');
var contentArray = new Array(' ');
var creatorArray = new Array(" ");


const API = 'https://uh29wea890.execute-api.us-east-2.amazonaws.com/TestingBubbleVersion1';
// const IMAGES = 'https://sanomind-audio-and-image-assets.s3.us-east-2.amazonaws.com/Images/';
const IMAGES = 'https://sanomind.com/MobileApp/image/items/';
// const AUDIO = 'https://sanomind-audio-and-image-assets.s3.us-east-2.amazonaws.com/Audio/';
const AUDIO = 'https://sanomind.com/MobileApp/audio/';


// Creating HTML from API
function addCategory (categoryTitle, categoryId) 
{ 
    // create a new div element 
    var mindsnackDiv = document.createElement("div"); 
    mindsnackDiv.className += 'mindSnackSection';
    mindsnackDiv.id += 'mindSnackSection-' + categoryTitle;

    var categoryH1Row = document.createElement("div"); 
    categoryH1Row.className += 'row' + ' category';
    var categoryH1 = document.createElement("h1"); 
    categoryH1.className += 'category-text';
    var headerText = document.createTextNode(categoryTitle);

    categoryH1.appendChild(headerText);
    categoryH1Row.appendChild(categoryH1);
    // mindsnackDiv.appendChild(categoryH1Row);
    

    var snackDiv = document.createElement('div');
    snackDiv.className += "mainSnackDiv row flex-nowrap flex-row";
    snackDiv.id += "mainSnackDiv-" + categoryId;

    
    mindsnackDiv.appendChild(categoryH1Row);
    mindsnackDiv.appendChild(snackDiv);

    
    // var parentDiv = document.getElementById('meetTheExpertsSection');
    var parentDiv = document.getElementById('content');
    parentDiv.appendChild(mindsnackDiv);


    // TESTING
    // This is to insert a Node, Div, after some known refrence node.
    // insertAfter(mindsnackDiv, parentDiv);
}

function addMindSnack(contentAudioFile, contentTitle, contentCoverArt, creatorId, insertAtDivId)
{
    // Add a mind snack to a category row

    // Find Content Creator
    // const creator = searchForCreator(creatorId);

    var creator;

    // console.log("Creator Array:", creatorArray);
    creatorArray.forEach((element) => {
        if (creatorId === element.Id) {
            console.log(element);
            creator = element;
        }
      })

    // console.log("Creater Object: ", creator);


    var mindSnackColumn = document.createElement('div');
    mindSnackColumn.className+='col-xs-2' + ' col-sm-2' + ' col-md-2' + ' col-lg-2' + ' snacks';

    var thumbnailDiv = document.createElement('div');
    thumbnailDiv.className += 'thumbnail';


    var anchorForCover = document.createElement('a');
    // anchorForCover.href = '';
    anchorForCover.addEventListener("click", function(){ playClickedMindsnack(contentAudioFile, creator, contentTitle, contentCoverArt);} );

    var mindSnackCover = document.createElement('img');
    mindSnackCover.id += 'cover-image';
    // console.log(contentCoverArt);

    mindSnackCover.src = IMAGES + contentCoverArt;
    mindSnackCover.alt = contentTitle + " Cover Image";

    anchorForCover.appendChild(mindSnackCover);

    var anchorForTitle = document.createElement('a');
    anchorForTitle.href = '#modal-id';
    anchorForTitle.className += 'titleAnchor'
    anchorForTitle.setAttribute('data-toggle', 'modal');
    anchorForTitle.setAttribute('data-placement', 'top');
    anchorForTitle.setAttribute('title', contentTitle);
    anchorForTitle.onmouseover += $('.titleAnchor').tooltip({ boundary: 'window' });
    
    anchorForTitle.addEventListener('click', function(){ updateModal(creator, IMAGES + contentCoverArt, contentTitle);});

    // var mindSnackTitle= document.createElement('h5');
    var titleText = document.createTextNode(contentTitle);
    anchorForTitle.appendChild(titleText);
    // Original
    // mindSnackTitle.appendChild(titleText);
    // anchorForTitle.appendChild(mindSnackTitle);

    var anchorForCreator = document.createElement('a');
    anchorForCreator.href = '#modal-id';
    anchorForCreator.className += 'creatorAnchor'
    anchorForCreator.setAttribute('data-toggle', 'modal');
    anchorForCreator.addEventListener('click', function(){ updateModal(creator, IMAGES + contentCoverArt, contentTitle);});

    var mindSnackCreator = document.createElement('p');
    var creatorText = document.createTextNode(creator.Name);
    mindSnackCreator.appendChild(creatorText);
    mindSnackCreator.className += 'text-muted';
    anchorForCreator.appendChild(mindSnackCreator);

    // Appending each Element

    mindSnackColumn.appendChild(thumbnailDiv);
    thumbnailDiv.appendChild(anchorForCover);
    thumbnailDiv.appendChild(anchorForTitle);
    thumbnailDiv.appendChild(anchorForCreator);

    var insertMindSnackAt = document.getElementById("mainSnackDiv-" + insertAtDivId);

    insertMindSnackAt.appendChild(mindSnackColumn);

}


// Function to insert content AFTER an Existing Node.
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

// ____________________________
// Mind Snack Functions - START
// ----------------------------
function playClickedMindsnack(audioId, creator, contentTitle, coverImg)
{
    
    var audioPlayer = document.getElementById('audioPlayer'); 
    audioPlayer.src = AUDIO + audioId;
    audioPlayer.load();

    var coverAnchor = document.getElementById('imgAnchor');
    coverAnchor.addEventListener('click', function(){updateModal(creator, coverImg, contentTitle);});
    var cover = document.getElementById('audioImage');
    // cover.src = IMAGES + coverImg;

    var contentNameText = document.getElementById('audioPlayerContentName');
    contentNameText.addEventListener('click', function(){updateModal(creator, coverImg, contentTitle);});
    // contentNameText.innerText = contentTitle;

    var creatorNameText = document.getElementById('audioPlayerCreatorName');
    creatorNameText.addEventListener('click', function(){updateModal(creator, coverImg, contentTitle);});
    // creatorNameText.innerText = creatorName;


    var promise = playaudio();
    if (promise !== undefined) {
        promise.then(_ => {
            // Autoplay started!
            cover.src = IMAGES + coverImg;
            // console.log(IMAGES + coverImg);
            contentNameText.innerText = contentTitle;
            creatorNameText.innerText = creator.Name;
        }).catch(error => {
            // Autoplay was prevented. Show a "Play" button so that user can start playback.
        });
    }
    
    

}

function updateModal(creator, contentCoverImage, contentName)
{
    // console.log("Result of content URL:",contentCoverImage);
    // console.log("Creater Object: ", creator);
    // (creatorName, creatorBio, creatorImage, contentCoverImage, contentName)
    // Create loop that checks all creatorArray[0].Name === creatorName
    // Dont need all the parameter since we fetched an object with all that data
    var modalTitle = document.getElementById('creatorName');
    modalTitle.innerText = creator.Name;

    var creatorImg = document.getElementById('creatorImage');
    creatorImg.src = IMAGES + creator.Image;

    var creatorBioText = document.getElementById('creatorBio');
    creatorBioText.innerText = creator.Bio;

    // var contentImg = document.getElementsByClassName("contentImg");
    // console.log(contentImg);
    // contentImg.src = contentCoverImage;

    $(".contentImg").attr("src",contentCoverImage);
    $(".contentImg").attr("alt",contentName);

    

    var contentTitle = document.getElementById('contentTitle');
    contentTitle.innerText = contentName;

}
// Mind Snack Functions - END
//---------------------


// ______________________________
// Audio Player Functions - START
// ------------------------------
function formatTime(seconds) 
{
    minutes = Math.floor(seconds / 60);
    minutes = (minutes >= 10) ? minutes : "0" + minutes;
    seconds = Math.floor(seconds % 60);
    seconds = (seconds >= 10) ? seconds : "0" + seconds;
    return minutes + ":" + seconds;
}
       
function setupSeekbar() 
{
    var audio = document.getElementById("audioPlayer");
    var seekbar = document.getElementById('seekbar');
    seekbar.min = audio.startTime;
    seekbar.max = audio.startTime + audio.duration;
}

function updateUI() 
{
    var audio = document.getElementById("audioPlayer");
    var seekbar = document.getElementById('seekbar');
    var startTime = document.getElementById('currentTime');
    // var endTime = document.getElementById('endTime');
    // var lastBuffered = audio.buffered.end(audio.buffered.length-1);
    seekbar.min = audio.startTime;
    startTime.innerText = formatTime(audio.currentTime) + ' / ' + formatTime(audio.duration);
    // endTime.innerText = '/ ' + audio.duration;
    seekbar.max = audio.duration;
    seekbar.value = audio.currentTime;
}

function changeVolume()
{
    var audio = document.getElementById("audioPlayer");
    var volumebar = document.getElementById('volumebar');
    audio.volume = volumebar.value/100;
}

function seekAudio()
{
    var audio = document.getElementById("audioPlayer");
    var seekbar = document.getElementById('seekbar');
    audio.currentTime = seekbar.value;
}

function forward15sec()
{
    var audio = document.getElementById("audioPlayer");
    audio.currentTime += 15.00;
}

function rewind15sec()
{
    var audio = document.getElementById("audioPlayer");
    audio.currentTime -= 15.00;
}

function playaudio()
{

    var x = document.getElementById("audioPlayer"); 
    
    var glyph = document.getElementById("togglePlay"); 
    glyph.className = "glyphicon glyphicon-pause";
    
    return x.play();
}

function pauseaudio()
{
    var x = document.getElementById("audioPlayer"); 

    var glyph = document.getElementById("togglePlay"); 
    glyph.className = "glyphicon glyphicon-play";

    return x.pause();
}

function togglePlay() {
    var myAudio = document.getElementById("audioPlayer"); 
    return myAudio.paused ? playaudio() : pauseaudio();
  };
// Audio Player Functions - END
// ----------------------------


// _________________________
// Objects for Data from API
// ------------------------- 
class MindSnack {
    constructor(Title, TherapistId, AudioFile, CoverArt, Category) 
    {
        this.Title = Title;
        this.TherapistId = TherapistId;
        this.AudioFile = AudioFile;
        this.CoverArt = CoverArt;
        this.Category = Category;
    }
}

class Creator {
    constructor(Name, Website, Bio, Image, Id) 
    {
        this.Name = Name;
        this.Website = Website;
        this.Bio = Bio;
        this.Image = Image;
        this.Id = Id;
    }
}

class Category {
    constructor(title, id) 
    {
        this.title = title;
        this.id = id;
    }
}


// ___________
// API Calls
// -----------
function getCategories() 
{
    // GET: API + '/mindsnacks/snackcategory';
    const requestURL = "/mindsnacks/snackcategory";

    fetch(API + requestURL)
    .then((res) => res.json())
    .then((data) => {
        data.forEach(element => {
            // console.log("Category ",element);
            // console.log('Category Title: ', element.categoryTitle)
            categoryArray.push(new Category(element.categoryTitle, element.id))
        });
    })
    .then(function(){getMindSnacks();})
    .catch((err) => {console.log('Error:',err);});

}

function getMindSnacks()
{
    // GET: API + '/mindsnacks/snackcategory';
    const requestURL = "/mindsnacks";

    fetch(API + requestURL)
    .then((res) => res.json())
    .then((data) => {
        data.forEach(element => {
            // console.log("MindSnack: ",element);
            // console.log('Category Title: ', element.categoryTitle)
            // console.log('Category Title: ', element.categoryTitle)
            contentArray.unshift(new MindSnack(element.snackTitle, element.therapistId, element.audio, element.cover, element.categoryId));
        });
    })
    .then(function(){getAllMindSnackCreator();})
    .catch((err) => {console.log('Error:',err);});

    // createCategorySections();
}

// function createCategorySections(categoryElement)
// {
//     // After each Section is created call addSnacksToCategorySection?

//     addCategory(categoryElement.title, categoryElement.id);
//     addSnacksToCategorySection(categoryElement.id);

// }

function createCategorySections() 
{

    categoryArray.shift();
    // console.log("Category Array:", categoryArray.length);
    // console.log("Category Array:", categoryArray);
    
    contentArray.shift();
    // console.log("Content Array:", contentArray.length);
    creatorArray.shift();
    // console.log("Creator Array:", creatorArray.length);
    // console.log("Creator Array:", creatorArray);
    

    // getAllMindSnackCreator();

    // console.log("Category Array length: ",categoryArray.length);

    for (let i = 0; i < categoryArray.length; i++) 
    {
        addCategory(categoryArray[i].title, categoryArray[i].id);
        addSnacksToCategorySection(categoryArray[i].id);
    }

    

    
}

// function createCategorySections(categoryElement)
// {
//     // After each Section is created call addSnacksToCategorySection?

//     addCategory(categoryElement.title, categoryElement.id);
//     addSnacksToCategorySection(categoryElement.id);

// }

// Make this a class method
function searchForCreator(id)
{   
    // console.log(id);

    creatorArray.forEach((element) => {
        if (id === element.Id) {
            // console.log(element);
            return element;
        }
      })

      
    //   console.log("failed search!");
      return;



    // const creatorObj = creatorArray.find( ({ Id }) => Id === 'id' );

    // console.log(creatorObj);

    // return creatorObj;

    // for (let i = 0; i < creatorArray.length; i++) {
    //     const element = creatorArray[i];
        
    //     if (element.Id === id)
    //     {
    //         return 
    //     }
    // }
    // for (const key in creatorArray) {
    //     // console.log("key:", key);
    //     console.log("Value of creatorArray[key].Id: ", creatorArray[key].Id);
    //     if (creatorArray[key].Id === id)
    //     {
    //         console.log("key:", key);
    //         // console.log("Value of creatorArray[key]: ",creatorArray[key]);
    //         return creatorArray[key];
    //     }

    //     console.log("Not Found", id);
        
    // }
}


function addSnacksToCategorySection(categoryId)
{
    // Look through each item in the array of content (Which is sorted by category ID so it loads page from highest priority)
    // Add the content to the div of the category

    // I already wrote the function for this so just need update the method where the content is:

    for (let index = 0; index < contentArray.length; index++) 
    {
        const element = contentArray[index];

        // console.log('element:', element);

        if (element.Category === categoryId) 
        {
            // console.log(element.TherapistId);
            addMindSnack(element.AudioFile, element.Title, element.CoverArt, element.TherapistId, element.Category)
        }
        
    }
    // addMindSnack(element.audio, element.snackTitle, element.cover, element.therapistId, categoryId)

}


// function addSnacksToCategorySection(categoryId)
// {
//     // Loop through categoryArray and trigger API call for snack of Category
//     // contentArray = API call for Snack of that category
//     // Loop through contentArray
//     // Then, Use addMindSnack()
//     // 
//     // POST: API + '/mindsnacks/mind-snack-by-category'; 
//     // JSON Body:
//     // {
//     //     "requestType": "snacksByCategory",
//     //     "id": 5
//     // }

//     const requestURL = '/mindsnacks/mind-snack-by-category';
//     var toSend = {
//         requestType: "snacksByCategory",
//         id: categoryId
//     }

//     var jsonString = JSON.stringify(toSend);

//     fetch(API + requestURL, {method: 'POST', headers: {'Content-Type':'application/json'}, body: jsonString})
//     .then((response) => response.json())
//     .then((data) => {
//         // console.log('Success:', data);
//         // Change this to use an object
//         data.forEach(element => {
//             // console.log('Therapist Id: ', element.therapistId);
//             // console.log('Audio File: ', element.audio);
//             // console.log('Snack Title: ', element.snackTitle);
//             // console.log('Snack Title: ', element.cover);
//             // console.log('Category Id: ', element.categoryId);

//             addMindSnack(element.audio, element.snackTitle, element.cover, element.therapistId, categoryId);
//             // searchForCreator(element.therapistId);
//         });
        

//         // addMindSnack(data.audio, data.snackTitle, data.cover, data.therapistId, categoryId);
//         // searchForCreator(data.therapistId);
//     }).catch((error) => {
//         console.log('Error:', error);
//     });
    
// }

function getAllMindSnackCreator()
{
    // Just get all the Therapists and stored in Global Array
    // GET: API + '/therapist';
    // var creators = new Array(' ');
    const requestURL = '/therapist';

    fetch(API + requestURL)
    .then((res) => res.json())
    .then((data) => {
        data.forEach(element => {
            // console.log("ID: ",element.id);
            // console.log('Creator Name: ', element.name);
            // console.log('Creator Bio: ', element.description);
            // console.log('Creator Image: ', element.image);
            // console.log('Creator Website: ', element.website);

            // console.log("Length of Categories:", );
            
            creatorArray.push(new Creator(element.name, element.website, element.description, element.image, element.id));
        });
    })
    .then(function(){createCategorySections();})
    .catch((err) => {console.log('Error:',err);});
}

(function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))window.location=b})(navigator.userAgent||navigator.vendor||window.opera,'https://bit.ly/getsanomindapp');